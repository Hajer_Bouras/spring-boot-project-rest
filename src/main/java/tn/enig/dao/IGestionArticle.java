package tn.enig.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tn.enig.model.Article;

public interface IGestionArticle extends JpaRepository<Article, Integer> {
	
	public List<Article> getAllArticleBylotId(@Param("x") Integer id); 

}
