package tn.enig.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.enig.model.Lot;

@Repository
public interface IGestionLot extends JpaRepository<Lot, Integer> {

}
