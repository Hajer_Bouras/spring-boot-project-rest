package tn.enig.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "lot")
public class Lot {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private int numLot;
	private int numDA;
	
	public Lot() {
		super();
	}
	
	public Lot(Integer id, int numLot, int numDA) {
		super();
		this.id = id;
		this.numLot = numLot;
		this.numDA = numDA;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getNumLot() {
		return numLot;
	}

	public void setNumLot(int numLot) {
		this.numLot = numLot;
	}

	public int getNumDA() {
		return numDA;
	}

	public void setNumDA(int numDA) {
		this.numDA = numDA;
	}

	@Override
	public String toString() {
		return "Lot [id=" + id + ", numLot=" + numLot + ", numDA=" + numDA + "]";
	}
	
	
	
	

}
