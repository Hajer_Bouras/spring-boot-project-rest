package tn.enig.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tn.enig.dao.IGestionArticle;
import tn.enig.dao.IGestionLot;
import tn.enig.model.Article;
import tn.enig.model.Lot;

@RestController
public class AppRest {
	
	@Autowired
	IGestionArticle daoA ;
	
	@Autowired
	IGestionLot daoL;
	
	public void setDaoA(IGestionArticle daoA) {
		this.daoA = daoA;
	}
	public void setDaoL(IGestionLot daoL) {
		this.daoL = daoL;
	}

	@GetMapping("/lot")
	public List<Lot> get1() {
	return daoL.findAll(); 
	}
	
	
	@GetMapping("/article")
	public List<Article> get2() {
	return daoA.findAll(); 
	}
	
	
	@PostMapping("/ajouterArticle")
	public void addArticle(@RequestBody Article art) {
		daoA.save(art);	
	}
	
	
	@PostMapping("/ajouterLot")
	public void addLot(@RequestBody Lot lo) {
		daoL.save(lo);
	}
	
	
	@DeleteMapping("/deleteArticle/{id}")
	public void deleteArticle(@PathVariable("id") int id) {
		daoA.deleteById(id);
	}
	@DeleteMapping("/deleteLot/{id}")
	public void deleteLot(@PathVariable("id") int id) {
		daoL.deleteById(id);
	}
	
	

}
